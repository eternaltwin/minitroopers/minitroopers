export type ServerReadyResponse = {
  ready: boolean;
};
