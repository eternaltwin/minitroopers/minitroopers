import { Pipe, PipeTransform } from '@angular/core';
import {
  ItemSkill,
  PerkSkill,
  WeaponSkill,
} from '../../../../shared/src/skills.model';

@Pipe({
  standalone: true,
  name: 'getSkill',
})
export class GetSkillPipe implements PipeTransform {
  transform(
    skillId: string | null
  ): WeaponSkill | PerkSkill | ItemSkill | undefined {
    if (skillId != null) {
      return undefined;
    } else {
      return undefined;
    }
  }
}
