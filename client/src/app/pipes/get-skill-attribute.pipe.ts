import { Pipe, PipeTransform } from '@angular/core';
import { SkillName } from '@minitroopers/shared';

@Pipe({
  name: 'getSkillAttribute',
  standalone: true,
})
export class GetSkillAttributePipe implements PipeTransform {
  transform(skillName: SkillName, attribute: string): unknown {
    return skillName + '.' + attribute;
  }
}
