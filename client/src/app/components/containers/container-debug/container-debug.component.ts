import {
  AfterViewInit,
  Component,
  ElementRef,
  QueryList,
  ViewChildren,
} from '@angular/core';
import * as PIXI from 'pixi.js';

@Component({
  selector: 'app-debug',
  standalone: true,
  imports: [],
  templateUrl: './container-debug.component.html',
  styleUrl: './container-debug.component.scss',
})
export class DebugComponent implements AfterViewInit {
  @ViewChildren('insertHere') children!: QueryList<ElementRef>;

  ngOnInit(): void {}

  ngAfterViewInit(): void {
    this.init();
  }

  init() {
    const app = new PIXI.Application();

    app.init({ width: 960, height: 540 }).then(async () => {
      this.children?.first.nativeElement.appendChild(app.canvas);

      const container = new PIXI.Container();
      const trooperContainer = new PIXI.Container();
      container.addChild(trooperContainer);

      const bg = new PIXI.Sprite(
        await PIXI.Assets.load('assets/images/pixi/backgrounds/garden.jpg'),
      );
      bg.width = app.screen.width;
      bg.height = app.screen.height;
      app.stage.addChild(bg);

      const red = parseInt('f18e68', 16);
      const skin = parseInt('da9477', 16);

      const head = new PIXI.Sprite(
        await PIXI.Assets.load('assets/images/pixi/trooper/26.png'),
      );
      head.tint = skin;
      head.x = app.screen.width / 2 + 1;
      head.y = app.screen.height / 2 + 2;
      head.scale = 0.43;
      trooperContainer.addChild(head);

      const eye1 = new PIXI.Sprite(
        await PIXI.Assets.load('assets/images/pixi/trooper/29.png'),
      );
      eye1.x = app.screen.width / 2 + 12;
      eye1.y = app.screen.height / 2 + 15;
      trooperContainer.addChild(eye1);

      // const eye2 = new PIXI.Sprite(
      //   await PIXI.Assets.load('assets/images/pixi/trooper/29.png'),
      // );
      // eye2.x = app.screen.width / 2 + 1;
      // eye2.y = app.screen.height / 2 + 20;
      // trooperContainer.addChild(eye2);

      const body = new PIXI.Sprite(
        await PIXI.Assets.load('assets/images/pixi/trooper/15.png'),
      );
      body.tint = red;
      body.x = app.screen.width / 2 + 30;
      body.y = app.screen.height / 2 + 20;
      trooperContainer.addChild(body);

      const gun = new PIXI.Sprite(
        await PIXI.Assets.load('assets/images/pixi/weapons/540.png'),
      );
      gun.x = app.screen.width / 2;
      gun.y = app.screen.height / 2 + 50;
      trooperContainer.addChild(gun);

      const bre = new PIXI.Sprite(
        await PIXI.Assets.load('assets/images/pixi/accessories/40.png'),
      );
      bre.x = app.screen.width / 2 + 4;
      bre.y = app.screen.height / 2 + 14;
      trooperContainer.addChild(bre);

      const helmet = new PIXI.Sprite(
        await PIXI.Assets.load('assets/images/pixi/accessories/46.svg'),
      );
      helmet.tint = red;
      helmet.x = app.screen.width / 2;
      helmet.y = app.screen.height / 2;
      trooperContainer.addChild(helmet);

      app.stage.addChild(container);
    });
  }
}
