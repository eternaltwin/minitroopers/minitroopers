import { CommonModule } from '@angular/common';
import { Component, Input } from '@angular/core';
import { SkillName } from '@minitroopers/shared';
import { TooltipDirective } from 'src/app/directives/tooltip.directive';

@Component({
  selector: 'app-skills',
  standalone: true,
  imports: [CommonModule, TooltipDirective],
  templateUrl: './skills.component.html',
  styleUrl: './skills.component.scss',
})
export class SkillsComponent {
  @Input() skillName: SkillName | undefined = undefined;
}
