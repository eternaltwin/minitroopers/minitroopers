import { CommonModule } from '@angular/common';
import { Component, Input } from '@angular/core';
import { TrooperCellComponent } from '../trooper-cell/trooper-cell.component';
import { FormGroup } from '@angular/forms';
import { Trooper, TrooperDay } from '@minitroopers/prisma';

@Component({
  selector: 'app-choose-trooper',
  standalone: true,
  imports: [CommonModule, TrooperCellComponent],
  templateUrl: './choose-trooper.component.html',
  styleUrl: './choose-trooper.component.scss',
})
export class ChooseTrooperComponent {
  @Input() troopers: TrooperDay[] = [];
  @Input() formGroup!: FormGroup<string>;

  selectedTrooper: string | undefined = undefined;

  onSelect(trooperId: string) {
    this.selectedTrooper = trooperId;
    this.formGroup.get('trooper')?.setValue(trooperId);
  }
}
